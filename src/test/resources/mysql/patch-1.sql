DROP TABLE IF EXISTS test1;
DROP TABLE IF EXISTS test2;
CREATE TABLE test1 (id int(10), title varchar(100), description varchar(1024));
CREATE TABLE test2 (id int(10), first_name varchar(50), last_name varchar(50));