DROP TABLE IF EXISTS test1;
DROP TABLE IF EXISTS test2;
CREATE TABLE test1 (id smallint, title varchar(100), description varchar(1024));
CREATE TABLE test2 (id smallint, first_name varchar(50), last_name varchar(50));