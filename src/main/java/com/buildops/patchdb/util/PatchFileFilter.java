package com.buildops.patchdb.util;

import java.io.File;
import java.io.FilenameFilter;

public final class PatchFileFilter implements FilenameFilter {
    private String basename = null;

    private String extension = null;

    public PatchFileFilter(final String newBasename, final String newExtension) {
        this.basename = newBasename;
        this.extension = newExtension;
    }

    @Override
    public boolean accept(final File dir, final String name) {
        File file = new File(dir, name);

        return (file.exists() && file.isFile() && name.startsWith(basename + '-') && name.endsWith("."
                + extension));
    }

    public String getBasename() {
        return basename;
    }

    public String getExtension() {
        return extension;
    }

    public void setBasename(final String newBasename) {
        this.basename = newBasename;
    }

    public void setExtension(final String newExtension) {
        this.extension = newExtension;
    }
}
