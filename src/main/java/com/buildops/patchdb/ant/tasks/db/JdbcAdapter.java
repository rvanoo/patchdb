package com.buildops.patchdb.ant.tasks.db;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Interface to implement adapters for various RDMNS systems that PatchDB needs to integrate with
 * 
 * @author rob
 * 
 */
public interface JdbcAdapter {
    /**
     * Gets the current patch level from the DB
     * 
     * @param connection
     *            DB connection to use
     * @param patchTable
     *            name of the patch table
     * @param levelColumn
     *            name of the patch level column
     * @return
     * @throws SQLException
     */
    int getCurrentPatchLevel(Connection connection, String patchTable, String levelColumn)
            throws SQLException;

    /**
     * Initializes the target database by creating the patch level table/column
     * 
     * @param connection
     * @param patchTable
     * @param levelColumn
     * @throws SQLException
     */
    void initialize(Connection connection, String patchTable, String levelColumn) throws SQLException;

    /**
     * Updates the patch level to the given new level
     * 
     * @param connection
     * @param patchTable
     * @param levelColumn
     * @param newLevel
     * @throws SQLException
     */
    void updatePatchLevel(final Connection connection, final String patchTable, final String levelColumn,
            final int newLevel) throws SQLException;

    /**
     * Checks whether a line of text is commentary.
     * 
     * @param text
     * @return true if the text provided is a commentary line
     */
    boolean isCommentary(final String text);
}
