package com.buildops.patchdb.ant.tasks.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class MySQLAdapter implements JdbcAdapter {
    @Override
    public int getCurrentPatchLevel(final Connection connection, final String patchTable,
            final String levelColumn) throws SQLException {
        ResultSet result = connection
                .createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY).executeQuery(
                        "SELECT " + levelColumn + " FROM " + patchTable + ';');

        result.next();
        return result.getInt(levelColumn);
    }

    public void initialize(final Connection connection, final String patchTable, final String levelColumn)
            throws SQLException {
        connection.createStatement().execute(
                "CREATE TABLE IF NOT EXISTS " + patchTable + '(' + levelColumn + " int(10));");
        connection.createStatement().execute(
                "INSERT INTO " + patchTable + " (" + levelColumn + ") VALUE (-1);");
    }

    public void updatePatchLevel(final Connection connection, final String patchTable,
            final String levelColumn, final int newLevel) throws SQLException {
        connection.createStatement().execute(
                "UPDATE " + patchTable + " SET " + levelColumn + " = " + newLevel + ';');
    }

    public boolean isCommentary(final String text) {
        return text.trim().startsWith("--");
    }
}
