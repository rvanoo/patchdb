package com.buildops.patchdb.ant.tasks.db;

import org.apache.tools.ant.BuildException;

public class JdbcAdapterFactory {
    protected JdbcAdapterFactory() {
        super();
    }

    public static JdbcAdapter getInstanceFor(final String identifier) {
        if ("mysql".equals(identifier)) {
            return new MySQLAdapter();
        } else if ("postgresql".equals(identifier)) {
            return new PostgreSQLAdapter();
        }

        throw new BuildException("Unsupported database: " + identifier);
    }
}
