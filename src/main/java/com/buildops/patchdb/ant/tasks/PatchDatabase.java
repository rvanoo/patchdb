package com.buildops.patchdb.ant.tasks;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.sql.Statement;
import java.util.StringTokenizer;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.LogLevel;

import com.buildops.patchdb.ant.tasks.db.JdbcAdapter;
import com.buildops.patchdb.ant.tasks.db.JdbcAdapterFactory;
import com.buildops.patchdb.util.PatchFileFilter;


public final class PatchDatabase extends Task {
    private JdbcAdapter adapter = null;

    private int assumelevel = -1;

    private String basename = "patch";

    private boolean clean = false;

    private String database = "mysql";

    private String driverClass = null;

    private String extension = "sql";

    private String levelColumn = "level";

    private boolean offline = false;

    private String outputFile = "patch.sql";

    private String password = "";

    private String patchdir = "sql";

    private String patchTable = "patch";

    private String url = null;
    
    private String user = null;

    public void execute() throws BuildException {
        // resolve database type used and bind the correct implementation of
        // JdbcAdapter
        String identifier = database;

        if (!offline) {
            StringTokenizer tokenizer = new StringTokenizer(url, ":");

            if (tokenizer.countTokens() == 3 && tokenizer.nextToken().equalsIgnoreCase("jdbc")) {
                identifier = tokenizer.nextToken();
            } else {
                throw new BuildException("Not a valid JDBC URL: " + url);
            }
        }

        adapter = JdbcAdapterFactory.getInstanceFor(identifier);

        try {
            int patchLevel;

            if (isOffline() && !isClean()) {
                patchLevel = assumelevel;
            } else {
                patchLevel = -1;
            }

            Connection connection = null;

            if (!offline) {
                Class.forName(driverClass);
                connection = DriverManager.getConnection(url, user, password);
            }

            if (connection != null || offline) {
                if (!isOffline()) {
                    // retrieve current patch level or initialize system
                    try {
                        patchLevel = adapter.getCurrentPatchLevel(connection, patchTable, levelColumn);
                        log("Found patch level: " + patchLevel, LogLevel.INFO.getLevel());
                    } catch (SQLException e) {
                        initialize(connection);
                    }
                }

                // retrieve available patches

                File patchDirectory = new File(patchdir);

                if (patchDirectory.exists() && patchDirectory.isDirectory()) {
                    File[] patchFiles = patchDirectory.listFiles(new PatchFileFilter(basename, extension));

                    log("Found total of " + patchFiles.length + " patches");

                    // sort patches just to make sure
                    patchFiles = sort(patchFiles);

                    // apply patches in order
                    
                    if (isClean()) {                        
                        log("Cleaning schema, starting from scratch ...");
                        patchLevel = -1;
                    }

                    Writer writer = null;

                    if (offline) {
                        File file = new File(outputFile);

                        if (file.exists() && file.isFile()) {
                            if (!file.delete()) {
                                throw new BuildException("output file exists and cannot be removed");
                            }

                            file = new File(outputFile);
                        }

                        writer = new BufferedWriter(new FileWriter(file));
                    }

                    for (int i = 0; i < patchFiles.length; i++) {
                        BufferedReader reader = null;

                        int patchNumber = Integer.parseInt(patchFiles[i].getName().substring(
                                basename.length() + 1, basename.length() + 2));

                        if (patchNumber > patchLevel) {
                            Statement statement = null;

                            if (!offline) {
                                statement = connection.createStatement();
                            }

                            reader = new BufferedReader(new FileReader(patchFiles[i]));
                            String text = null;

                            // repeat until all lines are read

                            while ((text = reader.readLine()) != null) {
                                // eliminate leading and/or trailing spaces
                                text = text.trim();

                                // skip commentary and empty lines
                                if (!text.isEmpty() && !adapter.isCommentary(text)) {
                                    // JDBC doesn't like multi-line statements
                                    while (!text.endsWith(";")) {
                                        text = text + ' ' + reader.readLine().trim();
                                    }

                                    if (offline) {
                                        log("writing: " + text);
                                        writeLineSeparator(writer, text);
                                    } else {
                                        statement.addBatch(text);
                                    }
                                }
                            }

                            log("Applying patch #" + patchNumber);
                            if (!offline) {
                                statement.executeBatch();
                            }

                            // update patch level

                            if (!offline) {
                                adapter.updatePatchLevel(connection, patchTable, levelColumn, patchNumber);
                            }

                            patchLevel = patchNumber;
                        }
                    }
                } else {
                    throw new BuildException("patchdir does not exist or is not a directory");
                }
            } else {
                throw new BuildException("No connection to database");
            }
        } catch (IllegalArgumentException e) {
            throw new BuildException(e);
        } catch (SecurityException e) {
            throw new BuildException(e);
        } catch (ClassNotFoundException e) {
            throw new BuildException(e);
        } catch (SQLException e) {
            throw new BuildException(e);
        } catch (FileNotFoundException e) {
            throw new BuildException(e);
        } catch (IOException e) {
            throw new BuildException(e);
        }
    }

    public boolean isClean() {
        return clean;
    }

    public void setClean(boolean clean) {
        this.clean = clean;
    }

    public int getAssumelevel() {
        return assumelevel;
    }

    public String getBasename() {
        return basename;
    }

    public String getDriverClass() {
        return driverClass;
    }

    public String getExtension() {
        return extension;
    }

    public String getPassword() {
        return password;
    }

    public String getPatchdir() {
        return patchdir;
    }

    public String getPatchTable() {
        return patchTable;
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    private void initialize(Connection connection) throws SQLException {
        log("Initializing PatchDB ...");
        adapter.initialize(connection, patchTable, levelColumn);
    }

    public boolean isOffline() {
        return offline;
    }

    public void setAssumelevel(final int newAssumelevel) {
        this.assumelevel = newAssumelevel;
    }

    public void setBasename(final String newBasename) {
        this.basename = newBasename;
    }

    public void setDriverClass(final String newDriver) {
        this.driverClass = newDriver;
    }

    public void setExtension(final String newExtension) {
        this.extension = newExtension;
    }

    public void setOffline(final boolean newOffline) {
        this.offline = newOffline;
    }

    public void setPassword(final String newPassword) {
        this.password = newPassword;
    }

    public void setPatchdir(final String newPatchdir) {
        this.patchdir = newPatchdir;
    }

    public void setPatchTable(final String newTable) {
        this.patchTable = newTable;
    }

    public void setUrl(final String newUrl) {
        this.url = newUrl;
    }

    public void setUser(final String newUsername) {
        this.user = newUsername;
    }

    private File[] sort(File[] patchFiles) {
        int n = patchFiles.length;

        do {
            int m = 0;

            for (int i = 0; i < n - 2; i++) {
                if (patchFiles[i].compareTo(patchFiles[i + 1]) > 0) {
                    patchFiles = swap(patchFiles, i, i + 1);
                    m = i + 1;
                }
            }

            n = m;
        } while (n > 1);
        return patchFiles;
    }

    private File[] swap(final File[] patchFiles, final int i, final int j) {
        File tempFile = patchFiles[i];
        patchFiles[i] = patchFiles[i + 1];
        patchFiles[i + 1] = tempFile;

        return patchFiles;
    }

    private void writeLineSeparator(Writer writer, String text) throws IOException {
        writer.write(text + System.getProperty("line.separator"));
        writer.flush();
    }
}
